﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elevator_B2
{
    class Person
    {
        public int CurrentFloor { get; private set; }

        public int WantsToGoToFloor { get; private set; }
        public int TimeWaitedForElevator { get; private set; }
        public int TimeWaitedInElevator { get; private set; }
        public int TotalTimeWaited { get { return TimeWaitedInElevator + TimeWaitedForElevator; } }

        public DirectionalModeEnum Direction{
            get
            {
                if (WantsToGoToFloor > CurrentFloor) { return DirectionalModeEnum.GOINGUP; }
                else { return DirectionalModeEnum.GOINGDOWN; };
            }
        }

        public bool IsAtDestinationFloor { get { return (WantsToGoToFloor == CurrentFloor); } }


        public Person(int startingFloor, int wantsToGoTofloor)
        {
            this.CurrentFloor = startingFloor;
            this.WantsToGoToFloor = wantsToGoTofloor;

            this.TimeWaitedForElevator = 0;
            this.TimeWaitedInElevator = 0;
        }

        public void WaitsForElevator()
        {
            TimeWaitedForElevator++;
        }
        public void WaitsInElevator()
        {
            TimeWaitedInElevator++;
        }

    }
}
