﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elevator_B2
{
    public enum DirectionalModeEnum
    {
        GOINGUP = 1, 
        GOINGDOWN = -1,
        NEUTRAL = 0
            
    }
}
