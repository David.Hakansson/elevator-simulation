﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elevator_B2
{
    class Elevator
    {
        
        public int CurrentFloor { get;  private set; } 
        public int Capacity { get; private set; }

        public List<Person> PeopleInElevator { get; private set; }
        public List<int> ButtonsPressedForFloors { get; private set; }

        public int TargetFloor { get; set; }
        public DirectionalModeEnum CurrentDirMode { get; set; }

        public int SystemTime { get; private set; }
        public bool IsFull { get { return(PeopleInElevator.Count >= Capacity); } }


        public Elevator(int capacity, int startingFloor, DirectionalModeEnum startingDirection)
        {
            this.CurrentFloor = startingFloor;
            this.Capacity = capacity;

            PeopleInElevator = new List<Person>();
            ButtonsPressedForFloors = new List<int>();
            CurrentDirMode = new DirectionalModeEnum();
            CurrentDirMode = startingDirection;

            SystemTime = 0;
        }
        public Elevator() : this(10, 0, DirectionalModeEnum.NEUTRAL)
        { 
            
        }

        public void EntersElevator(Person person)
        {
            PeopleInElevator.Add(person);
        }
        public void LeavesElevator(Person person)
        {
            PeopleInElevator.Remove(person);
        }
        public void PressButton(int buttonPressed){
            ButtonsPressedForFloors.Add(buttonPressed);
        }

        public void StoppedAtFloor(int floor) {
            foreach(int i in ButtonsPressedForFloors)
            {
                if(i == floor)
                {
                    ButtonsPressedForFloors.Remove(i);
                }
            }
        }

        public void MoveOneFloor(List<List<Person>> peopleInHouse)
        {
            foreach (List<Person> peopleOnFloor in peopleInHouse)
            {
                foreach (Person personOnFloor in peopleOnFloor)
                {
                    personOnFloor.WaitsForElevator();
                }
            }

            foreach (Person personInElevator in PeopleInElevator)
            {
                personInElevator.WaitsInElevator();
            }
            SystemTime++;
            peopleInHouse = Program.UpdateDataSet(peopleInHouse, SystemTime);
        }

        public void MoveUp(List<List<Person>> peopleInHouse)
        {
            MoveOneFloor(peopleInHouse);
            CurrentFloor++;

        }
        public void MoveDown(List<List<Person>> peopleInHouse)
        {
            MoveOneFloor(peopleInHouse);
            CurrentFloor--;

        }
        public void SetCurrentFloor(int currentFloor) 
        {
            this.CurrentFloor = currentFloor;
        }


    }
}
