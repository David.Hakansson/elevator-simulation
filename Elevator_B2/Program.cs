﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Elevator_B2
{
    class Program
    {
        public static List<List<List<Person>>> people = new List<List<List<Person>>>();
        static void Main(string[] args)
        {
            people = ReadDataSet(@"C:\Users\david\source\repos\Elevator_B2\test.txt");

            List<List<Person>> peopleT0 = people[0];

            House house = new House(peopleT0);
            house.PrintHouseCurrent();
            house.RunAlgorithm();

            Console.WriteLine(house.HouseElevator.SystemTime);

            List<Person> pp = house.PeopleWhoFinished;
            List<string> testfil = new List<string>();


            int sum = 0;
            int count = 0;


            foreach(Person p in pp)
            {
                count++;
                sum = sum + p.TotalTimeWaited;
                Console.WriteLine(p.TotalTimeWaited + " Total tid för Person " + count);
                Console.WriteLine(p.TimeWaitedForElevator + " Tid i huset för Person " + count);
                Console.WriteLine(p.TimeWaitedInElevator+ " Tid i hissen för Person " + count);

                testfil.Add(p.TotalTimeWaited + " Total tid för Person " + count + " " + p.TimeWaitedForElevator + " Tid i huset för Person " + count + " " + p.TimeWaitedInElevator + " Tid i hissen för Person " + count);
            }

            testfil.Add(pp.Count + " Antal som åkt");
            testfil.Add(sum / pp.Count + " Snitttid");

            

         


            

            System.IO.File.WriteAllLines(@"C:\Users\david\source\repos\Elevator_B2\WriteLines.txt", testfil);
        }
        //Läser datan ur CSV filen och läggen i en jagged lista där första dimenionen representerar våning och andra plats i kön till hissen
        public static List<List<List<Person>>> ReadDataSet(string path)
        {
            List<List<List<Person>>> fullList = new List<List<List<Person>>>();

            using (var reader = new StreamReader(path))
            {
    
                while (!reader.EndOfStream)
                {
                    int floor = 0;
                    List<List<Person>> TNList = new List<List<Person>>();
                    fullList.Add(TNList);
                    for (int i = 0; i < 10; i++)
                    {
                        int wantsToGoToFloor = new int();
                        List<Person> floorList = new List<Person>();

                        var line = reader.ReadLine();
                        var values = line.Split(',');

                        foreach (String s in values)
                        {
                            Int32.TryParse(s, out wantsToGoToFloor);
                            if (wantsToGoToFloor != -1 && wantsToGoToFloor != floor)
                            {
                                floorList.Add(new Person(floor, wantsToGoToFloor));
                            }
                        }

                        floor++;
                        TNList.Add(floorList);

                    }
                }
            }
            return fullList;
        }

        public static List<List<Person>> UpdateDataSet( List<List<Person>> peopleTN, int t)
        {
            if (people.Count > t)
            {
                List<List<Person>> newPeople = people[t];
            
                for (int i = 0; i < 10; i++)
                {
                    List<Person> peopleFloor = newPeople[i];
                    foreach (Person p in peopleFloor)
                    {
                        
                            peopleTN[i].Add(p);
              
                    }
                }
            }
            return peopleTN;
        }





    }
}

